/*
 * main.c
 * 
 * June 2013 - PCB-00620-06-1v0
 *
 * 
 */


#include "..\drivers\drivers.h"
#include <string.h>
#include <stdlib.h>
//#include <math.h>


typedef enum
{
	ST_INIT,
	ST_BAT_PRESENCE,
	ST_BAT_VERIF,
	ST_TRICKLE_CHARGE,
	ST_NORMAL_CHARGE,
	ST_BAT_PRECHARGE,
	ST_END_OF_CHARGE
}BAT_CH_STATES;

BAT_CH_STATES mainState;

typedef enum
{
	LED_IDLE,
	LED_ONE_SHOT,
	LED_TRICKLE_CH,
	LED_NORMAL_CH,
	LED_FAST_FLASH,
	LED_ALWAYS_ON
}LED_MODES;

LED_MODES	ledMode;

typedef enum
{
	LED_ON,
	LED_OFF
}LED_STATE;

LED_STATE ledState;

//WORD ledToggleTimes[5][2] =
//{
//		LED_ONE_SHOT_TIME,
//		0,
//		LED_TR_CH_ON_TIME,
//		LED_TR_CH_OFF_TIME,
//		LED_NOR_CH_ON_TIME,
//		LED_NOR_CH_OFF_TIME,
//		LED_FAST_ON_TIME,
//		LED_FAST_OFF_TIME,
//};

static TIMERHANDLE thGenTimer, thLedTimer;
static TIMERHANDLE thTimerSecError,thTimerSec2,thTimerSecEvents ;

WORD eventCounter;


UVOLTS adcChannelsData[MAX_IN_CHANNELS];
UVOLTS	adcChIsense;
UVOLTS	adcChInputV;
UVOLTS	adcChVfb;
UVOLTS	adcChBatDet;
UVOLTS vfbAverage;
UVOLTS vfbMax;
UVOLTS uVoltTmp1, uVoltTmp2;
UVOLTS uVoltDiff;
UVOLTS iSenseAverage;
UVOLTS vfbDiff;
UVOLTS vfbDiffMax;


BYTE batType;
WORD batPrechargeCounter;
///////////////////////////////////////////////////////////
//
// Local function declaration
//
///////////////////////////////////////////////////////////


void SystemInit(void);

BOOL CheckForBadCell(void);
void LedTask(void);
void SetLedTask(LED_MODES mode);
void EOCTask(void);
void NormalChargeTask(void);
void TrickleChargeTask(void);
void StartEOC(void);
void StartNormalCharge(void);
void StartTrickleCharge(void);
void BatVerifTask(void);
void BatPresTask(void);
void InitTask(void);
void MainTask(void);
BOOL CheckBatPresence(void);
BOOL CheckInputVoltage(void);
BOOL CheckForBatteryRemoval(void);
BOOL CheckForEndOfCharge(void);
void CalculateVFBMax(void);
void FilterAdcData(void);
void PowerDownTask(void);
void MeasureAllChannels(void);
void ResetAllVars(void);
void CheckForSystemErrors(void);
void SetAudibleAlarm(void);
void BuzzerTask(void);
void EnableCharger(void);
void SetBeep(void);
BOOL CheckFor6AHCell(void);
BOOL CheckFor4AHCell(void);
void StartPrecharge(void);
void BatPrechargeTask(void);


void main(void)
{

	StopWatchdog();

	SystemInit();

//	SetAudibleAlarm();	// test delete

	while(1)
	{

		if(TimerExpired(thGenTimer))
		{
			MeasureAllChannels();
			ResetTimer(thGenTimer, SAMPLE_TIME_MSEC);
			MainTask();
			CheckForSystemErrors();
		}
		LedTask();
		BuzzerTask();

#ifdef WATCHDOG_ENABLE
		ResetWatchdog();
#endif
	}



}



//******************************************************************************
// Function name:    SystemInit
//******************************************************************************
// Description:      Initialise the variables and setup the MCU
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SystemInit(void)
{
	mainState = ST_INIT;
	vfbAverage= 0;
	vfbMax = 0;
	ledState = LED_OFF;
	ledMode = LED_IDLE;
	vfbDiffMax = 0;
	batPrechargeCounter = 0;
	batType = BAT_UNDEF;
	__delay_cycles(50000);


#ifdef WATCHDOG_ENABLE
	SetWatchdog();
#endif

	initMPS430Pins();
//	SetOutputPin(LED_OUT,TRUE);
	BCSCTL1 = CALBC1_1MHZ;                    // Set DCO
	DCOCTL = CALDCO_1MHZ;
	P2DIR |= 0x03;                           // P2.0,1,2 output direction
	P2SEL |= 0x03;							 // P2.0 = ACLK, P2.1 = SMCLK
//	Set_System_Clock();
	InitTimers();
	__bis_SR_register(GIE);
	SpiB0Initialize();
	thGenTimer = RegisterTimer();
	ResetTimer(thGenTimer, 500);
	StartTimer( thGenTimer );
	thLedTimer = RegisterTimer();
	StartTimer( thLedTimer);

	thTimerSecError = RegisterSecTimer();
	ResetSecTimer(thTimerSecError , 1);
	StartSecTimer(thTimerSecError);
	thTimerSec2 = RegisterSecTimer();
	ResetSecTimer(thTimerSec2 , CHARGE_4AH_IN_SEC);
	StartSecTimer(thTimerSec2);

	thTimerSecEvents = RegisterSecTimer();
	StartSecTimer(thTimerSecEvents);


//	BuzzerTurnOn();
	while(!TimerExpired(thGenTimer))
	{
#ifdef WATCHDOG_ENABLE
		ResetWatchdog();
#endif
	}
//	BuzzerTurnOff();
	ResetTimer(thGenTimer, 2000);

//	SetOutputPin(LED_OUT,FALSE);

}


//******************************************************************************
// Function name:    MeasureAllChannels
//******************************************************************************
// Description:      Read the external 4ch. ADC. The data are in uVolts
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void MeasureAllChannels(void)
{

	ADS1118ReadVoltage(ISENSE , &adcChIsense);
	__delay_cycles(500);
	ADS1118ReadVoltage(VFB , &adcChVfb);
	__delay_cycles(500);
	ADS1118ReadVoltage(INPUT_V , &adcChInputV);
	__delay_cycles(500);
	ADS1118ReadVoltage(BAT_DETECT , &adcChBatDet);
	__delay_cycles(500);
	FilterAdcData();
	CalculateVFBMax();

}


//******************************************************************************
// Function name:    FilterAdcData
//******************************************************************************
// Description:       filter the ADC data
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void FilterAdcData(void)
{
	vfbAverage = (vfbAverage *3 + adcChVfb)/4;
	iSenseAverage = (iSenseAverage *3 + adcChIsense)/4;
}

//******************************************************************************
// Function name:    CalculateVFBMax
//******************************************************************************
// Description:       calculate the max VFB so far
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void CalculateVFBMax(void)
{
	if (vfbAverage >vfbMax)
		vfbMax = vfbAverage;
}

//******************************************************************************
// Function name:    CheckForEndOfCharge
//******************************************************************************
// Description:     End of charge condition for NiMH batteries	is
//					drop in the voltage 0.1% (delta V/V = 0.1%)
// parameters:      none
//
// Returned value:  none
//
//******************************************************************************
BOOL CheckForEndOfCharge(void)
{
	if(vfbMax>vfbAverage)
	{
//		if(vfbMax - vfbAverage >= BAT_EOC_DROP )
		vfbDiff = vfbMax - vfbAverage;
		if(vfbDiff > vfbDiffMax)
			vfbDiffMax= vfbDiff;

		if((vfbDiff > BAT_EOC_DROP )&&(vfbAverage>BAT_CHARGED_MIN_VOLTS))
		{
			eventCounter++;
		}
		else
		{
			eventCounter = 0;
		}

		if(eventCounter>EVENT_NBR_STABLE)
		{
			eventCounter = 0;
			return TRUE;
		}
	}

	return FALSE;
}

//******************************************************************************
// Function name:    CheckForBatteryRemoval
//******************************************************************************
// Description:     Battery removal.  During all charging states, monitor Isense.
//					If it drops to ~0, battery has been removed.
//					Reset timer and max Vbat and return to �Check for battery�
//
// parameters:      none
//
// Returned value:  none
//
//******************************************************************************
BOOL CheckForBatteryRemoval(void)
{
//	if(iSenseAverage<MIN_CHARGE_CURRENT)
	if(adcChIsense<MIN_CHARGE_CURRENT)
		return TRUE;
	else
		return FALSE;
}

//******************************************************************************
// Function name:    CheckInputVoltage
//******************************************************************************
// Description:     Verify that AC-DC input between 4 and 10 V
//
// parameters:      none
//
// Returned value:  none
//
//******************************************************************************
BOOL CheckInputVoltage(void)
{
	if(adcChInputV>= INPUT_MIN_VOLTS )
		return TRUE;
	else
		return FALSE;
}

//******************************************************************************
// Function name:    CheckBatPresence
//******************************************************************************
// Description:     Measure Vbat. If close to 0 then no battery is present.
//					If close to 1 V, then battery is present
//
// parameters:      none
//
// Returned value:  none
//
//******************************************************************************
BOOL CheckBatPresence(void)
{
	if(vfbAverage>= BAT_MIN_VOLTS )
		return TRUE;
	else
		return FALSE;
}


//******************************************************************************
// Function name:    MainTask
//******************************************************************************
// Description:		Main State machine. Controls the system's operation.
//
//
// parameters:      none
//
// Returned value:  none
//
//******************************************************************************
void MainTask(void)
{
	switch(mainState)
	{
	case ST_INIT:
		InitTask();
		break;
	case ST_BAT_PRESENCE:
		BatPresTask();
		break;
	case ST_BAT_VERIF:
		BatVerifTask();
		break;
	case ST_TRICKLE_CHARGE:
		TrickleChargeTask();
		break;
	case ST_NORMAL_CHARGE:
		NormalChargeTask();
		break;
	case ST_BAT_PRECHARGE:
		BatPrechargeTask();
		break;
	case ST_END_OF_CHARGE:
		EOCTask();
		break;
	default:
		mainState = ST_INIT;
		break;

	}
}

void InitTask(void)
{
	if(CheckInputVoltage())
	{
		mainState = ST_BAT_PRESENCE;
		SetOutputPin(VFB_SW,TRUE);
		SetLedTask(LED_ONE_SHOT);
		ResetTimer(thGenTimer, SOAK_UP_TIME);
		batPrechargeCounter = 0;
	}
	else
	{
		SetBeep();
	}

}

void BatPresTask(void)
{
	if(CheckBatPresence())
	{
		mainState = ST_BAT_VERIF;
		SetOutputPin(BAT_SW,TRUE);
		ResetTimer(thGenTimer, SOAK_UP_TIME);
		batType = BAT_UNDEF;
	}
	else
	{
		SetLedTask(LED_IDLE);
	}

}

void BatVerifTask(void)
{
	if(CheckFor6AHCell())
	{
		batType = BAT_NIMH6AH;
	}
	if(CheckFor4AHCell())
	{
		batType = BAT_NIMH4AH;
	}

	// check for Lithium battery or bad cell
	if(adcChBatDet > MAX_BAT_DETECT_V)
	{
		mainState = ST_BAT_PRESENCE;
		SetOutputPin(BAT_SW,FALSE);
		SetLedTask(LED_FAST_FLASH);
		ResetTimer(thGenTimer, ERROR_TIME_MSEC);
		SetAudibleAlarm();
		return;
	}
	else
	{
		// the battery may be in deep discharge state - precharge for a 3 sec 3 times and if it doesn't recover set the alarm.
		if(batType==BAT_UNDEF)
		{
			if(batPrechargeCounter<BAT_PRECHARGE_TIMES)
			{
				batPrechargeCounter++;
				StartPrecharge();
				return;

			}
			else
			{
				mainState = ST_BAT_PRESENCE;
				SetOutputPin(BAT_SW,FALSE);
				SetLedTask(LED_FAST_FLASH);
				ResetTimer(thGenTimer, ERROR_TIME_MSEC);
				SetAudibleAlarm();
				batPrechargeCounter = 0;
				return;
			}
		}
	}


	batPrechargeCounter = 0;
	// if battery is over-discharged go to trickle charge
	// else go to normal charge
	if(adcChVfb<= BAT_OVER_DISCHARGED)
	{
		StartTrickleCharge();
		return;
	}
	else
	{
		StartNormalCharge();
		return;
	}


}

void StartTrickleCharge(void)
{
	mainState = ST_TRICKLE_CHARGE;
	SetOutputPin(BAT_SW,FALSE);
	EnableCharger();
	SetLedTask(LED_TRICKLE_CH);

}

void StartNormalCharge(void)
{
	mainState = ST_NORMAL_CHARGE;
	SetOutputPin(BAT_SW,FALSE);
	EnableCharger();
	SetOutputPin(CHRG_RATE,TRUE);
	switch(batType)
	{
	case BAT_NIMH4AH:
		ResetSecTimer(thTimerSec2 , CHARGE_4AH_IN_SEC);
		break;
	case BAT_NIMH6AH:
		ResetSecTimer(thTimerSec2 , CHARGE_6AH_IN_SEC);
		break;
	default:
		ResetSecTimer(thTimerSec2 , CHARGE_6AH_IN_SEC);
		break;

	}

	SetLedTask(LED_NORMAL_CH);
	vfbMax = 0;
}

void StartEOC(void)
{
	mainState = ST_END_OF_CHARGE;
	SetOutputPin(BAT_SW,FALSE);
	EnableCharger();
	SetOutputPin(CHRG_RATE,FALSE);
	SetLedTask(LED_ALWAYS_ON);
}

void TrickleChargeTask(void)
{
	if(vfbAverage > BAT_OVER_DISCHARGED)
	{
		StartNormalCharge();
	}
	if(CheckForBatteryRemoval())
	{
		mainState = ST_BAT_PRESENCE;
		ResetAllVars();
		SetLedTask(LED_IDLE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(CHRG_RATE,FALSE);
		ResetTimer(thGenTimer, ERROR_TIME_MSEC);
		SetBeep();
	}
}


void NormalChargeTask(void)
{
	if(SecTimerExpired(thTimerSec2))
	{
		StartEOC();
	}

	if(CheckForEndOfCharge())
	{
		StartEOC();
	}
	if(CheckForBatteryRemoval())
	{
		mainState = ST_BAT_PRESENCE;
		ResetAllVars();
		SetLedTask(LED_IDLE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(CHRG_RATE,FALSE);
		ResetTimer(thGenTimer, ERROR_TIME_MSEC);
		SetBeep();
	}

}


void EOCTask(void)
{
	if(CheckForBatteryRemoval())
	{
		mainState = ST_BAT_PRESENCE;
		ResetAllVars();
		SetLedTask(LED_IDLE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(CHRG_RATE,FALSE);
		ResetTimer(thGenTimer, ERROR_TIME_MSEC);
		SetBeep();

	}
}


void SetLedTask(LED_MODES mode)
{
	ledMode = mode;

	switch(ledMode)
		{
		case LED_IDLE:
			ResetTimer(thLedTimer, 0);
			ledState = LED_OFF;
			SetOutputPin(LED_OUT,FALSE);
			break;
		case LED_ONE_SHOT:
			ResetTimer(thLedTimer, LED_ONE_SHOT_TIME);
			ledState = LED_ON;
			SetOutputPin(LED_OUT,TRUE);
			break;
		case LED_TRICKLE_CH:
			ResetTimer(thLedTimer, LED_TR_CH_ON_TIME);
			ledState = LED_ON;
			SetOutputPin(LED_OUT,TRUE);
			break;
		case LED_NORMAL_CH:
			ResetTimer(thLedTimer, LED_NOR_CH_ON_TIME);
			ledState = LED_ON;
			SetOutputPin(LED_OUT,TRUE);
			break;
		case LED_FAST_FLASH:
			ResetTimer(thLedTimer, LED_FAST_ON_TIME);
			ledState = LED_ON;
			SetOutputPin(LED_OUT,TRUE);
			break;
		case LED_ALWAYS_ON:
			ledState = LED_ON;
			SetOutputPin(LED_OUT,TRUE);
			break;
		default:
			ledMode = LED_IDLE;
			ledState = LED_OFF;
			ResetTimer(thLedTimer, 0);
			SetOutputPin(LED_OUT,FALSE);
			break;

		}

}

void LedTask(void)
{
	switch(ledMode)
	{
	case LED_IDLE:
		SetOutputPin(LED_OUT,FALSE);
		break;
	case LED_ONE_SHOT:
		if(TimerExpired(thLedTimer))
		{
			ledMode = LED_IDLE;
			ledState = LED_OFF;
		}
		break;
	case LED_TRICKLE_CH:
		switch(ledState)
		{
		case LED_ON:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_OFF;
				ResetTimer(thLedTimer, LED_TR_CH_OFF_TIME);
				SetOutputPin(LED_OUT,FALSE);
			}
			break;
		case LED_OFF:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_ON;
				ResetTimer(thLedTimer, LED_TR_CH_ON_TIME);
				SetOutputPin(LED_OUT,TRUE);
			}
			break;
		default:
			ledState = LED_OFF;
			break;
		}
		break;
	case LED_NORMAL_CH:
		switch(ledState)
		{
		case LED_ON:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_OFF;
				ResetTimer(thLedTimer, LED_NOR_CH_OFF_TIME);
				SetOutputPin(LED_OUT,FALSE);
			}
			break;
		case LED_OFF:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_ON;
				ResetTimer(thLedTimer, LED_NOR_CH_ON_TIME);
				SetOutputPin(LED_OUT,TRUE);
			}
			break;
		default:
			ledState = LED_OFF;
			break;
		}
		break;
	case LED_FAST_FLASH:
		switch(ledState)
		{
		case LED_ON:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_OFF;
				ResetTimer(thLedTimer, LED_FAST_OFF_TIME);
				SetOutputPin(LED_OUT,FALSE);
			}
			break;
		case LED_OFF:
			if(TimerExpired(thLedTimer))
			{
				ledState = LED_ON;
				ResetTimer(thLedTimer, LED_FAST_ON_TIME);
				SetOutputPin(LED_OUT,TRUE);
			}
			break;
		default:
			ledState = LED_OFF;
			break;
		}
		break;
	case LED_ALWAYS_ON:
		ledState = LED_ON;
		SetOutputPin(LED_OUT,TRUE);
		break;
	default:
		ledMode = LED_IDLE;
		break;

	}
}


BOOL CheckForBadCell(void)
{
	uVoltTmp1 = adcChVfb * SCALED_VBAT_DIV_3;
	uVoltTmp2 = adcChBatDet *10;
	if(uVoltTmp1 > uVoltTmp2)
	{
		if(uVoltTmp1 -uVoltTmp2 > BAD_CELL_TOLERANCE)
			return TRUE;		// bad cell
	}
	else
	{
		if(uVoltTmp2 -uVoltTmp1 > BAD_CELL_TOLERANCE)
			return TRUE;		// bad cell
	}

	return FALSE;				// good cell

}

//******************************************************************************
//
//  Function: CheckFor4AHCell
//
//  Arguments: void
//
//  Returns: void
//
//  Description:The 4AH battery has 10K connected to the first cell, which together
//				with R21=10K divides the cell voltage by 2.
//				The Vfb is scaled to Vbat/3 * 10
//				the Vbatdet is scaled to the Cell Voltage *10
//******************************************************************************
BOOL CheckFor4AHCell(void)
{
	uVoltTmp1 = adcChVfb * SCALED_VBAT_DIV_3;
	uVoltTmp2 = adcChBatDet *20;
	if(uVoltTmp1 > uVoltTmp2)
	{

//		if(uVoltTmp1 -uVoltTmp2 < BAD_CELL_TOLERANCE)
		uVoltDiff = uVoltTmp1 -uVoltTmp2 ;
		if(uVoltDiff < BAD_CELL_TOLERANCE)
			return TRUE;		// 4AH battery
	}
	else
	{
		uVoltDiff = uVoltTmp2 -uVoltTmp1;
//		if(uVoltTmp2 -uVoltTmp1 < BAD_CELL_TOLERANCE)
		if(uVoltDiff < BAD_CELL_TOLERANCE)
			return TRUE;		// 4AH battery
	}

	return FALSE;				// not a 4AH battery

}

//******************************************************************************
//
//  Function: CheckFor6AHCell
//
//  Arguments: void
//
//  Returns: void
//
//  Description:The 6AH battery has 20K connected to the first cell, which together
//				with R21=10K divides the cell voltage by 3.
//				The Vfb is scaled to Vbat/3 * 10
//				the Vbatdet is scaled to the Cell Voltage *10 = Vbatdet * 3 *10
//******************************************************************************
BOOL CheckFor6AHCell(void)
{
	uVoltTmp1 = adcChVfb * SCALED_VBAT_DIV_3;
	uVoltTmp2 = adcChBatDet *30;				// cell voltage is VbatDet*3
	if(uVoltTmp1 > uVoltTmp2)
	{
//		if(uVoltTmp1 -uVoltTmp2 < BAD_CELL_TOLERANCE)
		uVoltDiff = uVoltTmp1 -uVoltTmp2 ;
		if(uVoltDiff < BAD_CELL_TOLERANCE)
			return TRUE;		// 6AH battery
	}
	else
	{
//		if(uVoltTmp2 -uVoltTmp1 < BAD_CELL_TOLERANCE)
		uVoltDiff = uVoltTmp2 -uVoltTmp1;
		if(uVoltDiff < BAD_CELL_TOLERANCE)
			return TRUE;		// 6AH battery
	}

	return FALSE;				// not a 6AH battery

}



void ResetAllVars(void)
{
	vfbAverage = 0;
	vfbMax = 0;
	iSenseAverage = 0;
	vfbDiffMax = 0;

}

void CheckForSystemErrors(void)
{
	if(CheckInputVoltage()==FALSE)
	{
		mainState = ST_INIT;
		vfbAverage= 0;
		vfbMax = 0;
		ledState = LED_OFF;
		ledMode = LED_IDLE;
		vfbDiffMax = 0;
		SetOutputPin(BAT_SW,FALSE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(VFB_SW,FALSE);
	}
}


void BuzzerTask(void)
{
	if(SecTimerExpired(thTimerSecError))
	{
		BuzzerTurnOff();
	}
}

void SetAudibleAlarm(void)
{
	ResetSecTimer(thTimerSecError , ALARM_TIMEOUT_SEC);
	BuzzerTurnOn();
}

void SetBeep(void)
{
	ResetSecTimer(thTimerSecError , BEEP_TIMEOUT_SEC);
	BuzzerTurnOn();
}

//******************************************************************************
//
//  Function: EnableCharger
//
//  Arguments: void
//
//  Returns: void
//
//  Description: it enables the feedback first and then enables the charger.
//				Use this function only to enable the charger. Do not set the CHRG_EN only before making sure
//				the feedback is turned on - otherwise the charger doesn't have control over over the
//				output voltage and it rails at around 50V
//******************************************************************************
void EnableCharger(void)
{
	SetOutputPin(VFB_SW,TRUE);
	SetOutputPin(CHRG_EN,TRUE);
}



//******************************************************************************
//
//  Function: StartPrecharge
//
//  Arguments: void
//
//  Returns: void
//
//  Description: It starts the precharge cycle for PRECHARGE_IN_SEC time
//
//
//
//******************************************************************************
void StartPrecharge(void)
{
	mainState = ST_BAT_PRECHARGE;
	SetOutputPin(BAT_SW,FALSE);
	EnableCharger();
	SetOutputPin(CHRG_RATE,TRUE);
	ResetSecTimer(thTimerSec2 , PRECHARGE_IN_SEC);
	SetLedTask(LED_NORMAL_CH);
}

//******************************************************************************
//
//  Function: BatPrechargeTask
//
//  Arguments: void
//
//  Returns: void
//
//  Description: Run the precharge cycle for PRECHARGE_IN_SEC time or until the battery is removed.
//
//
//******************************************************************************
void BatPrechargeTask(void)
{
	if(SecTimerExpired(thTimerSec2))
	{
		mainState = ST_BAT_PRESENCE;
		ResetAllVars();
		SetLedTask(LED_IDLE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(CHRG_RATE,FALSE);
	}

	if(CheckForBatteryRemoval())
	{
		mainState = ST_BAT_PRESENCE;
		ResetAllVars();
		SetLedTask(LED_IDLE);
		SetOutputPin(CHRG_EN,FALSE);
		SetOutputPin(CHRG_RATE,FALSE);
		ResetTimer(thGenTimer, ERROR_TIME_MSEC);
		batPrechargeCounter = 0;
		SetBeep();
	}

}



/************************************
 ******* TEST FUNCTIONS**************
************************************/



