#ifndef UART_H_
#define UART_H_

#include "..\drivers.h"

//******************************************************************************
// TYPEDEFS
//******************************************************************************

// The UART_MGR_OBJECT is used by this module to maintain the context of an
// open serial port. This struct is required for the duration that a port is
// in use, therefore it should be allocated as a static variable.
typedef struct
{
   	const BYTE* ptrTxQ;                           // The UART manager controls these variables. Users of
    WORD  wTxQIndex;                              // this module must not directly modify these variables.
    WORD  wTxQSize;
    BOOL  bTxQSending;

} UART_OBJECT;

void UARTA0Init(void);
void UARTA0RxEnable(void);
void UARTA0TxEnable(void);
void UARTA0RxDisable(void);
void UARTA0TxDisable(void);
BOOL UARTA0SendBufferInt( const BYTE* pBuffer, WORD wLength );
void UARTA0SendBufferPol(const BYTE* pBuffer, WORD wLength );

void uart0TxISR( void );
BOOL IsZbTiSending(void);

#endif /*UART_H_*/
