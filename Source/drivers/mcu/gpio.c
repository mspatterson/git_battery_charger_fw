
//******************************************************************************
// Description:      initialize the MSP430 pins
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
#include "gpio.h"



// Direction registers table
static volatile BYTE * pPortDirRegs[] = {
	&P1DIR,
	&P2DIR,
	&P3DIR,
	&P4DIR,
};
// Output registers table
static volatile BYTE * pPortOutRegs[] = {
	&P1OUT,
	&P2OUT,
	&P3OUT,
	&P4OUT,
};

// Input registers table
static volatile BYTE * pPortInRegs[] = {
	&P1IN,
	&P2IN,
	&P3IN,
	&P4IN
};

// Selection register table
static volatile BYTE * pPortSelRegs[] = {
	&P1SEL,
	&P2SEL,
	&P3SEL,
	&P4SEL
};



// Resistor Enable register table
static volatile BYTE * pPortRenRegs[] = {
	&P1REN,
	&P2REN,
	&P3REN,
	&P4REN

};
//******************************************************************************
// Function name:    SetPinAsPeripheral
//******************************************************************************
// Description:      set the corresponding pin as peripheral
//
// parameters:       pinName
//
// Returned value:   none
//
//******************************************************************************
void SetPinAsPeripheral(BYTE pinName)
{
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		*(pPortSelRegs[portNumber]) |= bitMask;
	}
}

//******************************************************************************
// Function name:    SetPinAsOutput
//******************************************************************************
// Description:      set the corresponding pin as output
//
// parameters:       pinName
//
// Returned value:   none
//
//******************************************************************************
void SetPinAsOutput(BYTE pinName)
{
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		*(pPortSelRegs[portNumber]) &= ~bitMask;
		*(pPortDirRegs[portNumber]) |= bitMask;
	}
}


//******************************************************************************
// Function name:    SetPinAsInput
//******************************************************************************
// Description:      set the corresponding pin as input
//
// parameters:       pinName
//
// Returned value:   none
//
//******************************************************************************
void SetPinAsInput(BYTE pinName)
{
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		*(pPortSelRegs[portNumber]) &= ~bitMask;
		*(pPortDirRegs[portNumber]) &= ~bitMask;
	}
}

//******************************************************************************
// Function name:    SetOutputPin
//******************************************************************************
// Description:      set the corresponding pin logic level
//
// parameters:       pinName, logic state - TRUE =1, FALSE =0
//
// Returned value:   none
//
//******************************************************************************
void SetOutputPin(BYTE pinName, BOOL bState)
{
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		if (bState == TRUE)
		{
			*(pPortOutRegs[portNumber]) |= bitMask;
		}
		else if (bState == FALSE)
		{
			*(pPortOutRegs[portNumber]) &= ~bitMask;
		}
	}
}


//******************************************************************************
// Function name:    ReadInputPin
//******************************************************************************
// Description:      read the logic value of the corresponding pin
//
// parameters:       pinName
//
// Returned value:   logic state - TRUE =1, FALSE =0
//
//******************************************************************************
BOOL ReadInputPin(BYTE pinName)
{
	BYTE portNumber;
	BYTE pinNumber;
	volatile BYTE bitMask;
	volatile BYTE bState;
	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		bState = *pPortInRegs[portNumber];
		bState &= bitMask;

		if (bState)
		{
			return TRUE;
		}
		else if (bState == FALSE)
		{
			return FALSE;
		}
	}
	return FALSE;
}

//******************************************************************************
// Function name:    EnableInputPullUp
//******************************************************************************
// Description:      set the corresponding pin logic level
//
// parameters:       pinName, logic state - TRUE =1, FALSE =0
//
// Returned value:   none
//
//******************************************************************************
void EnableInputPullUp(BYTE pinName)
{
	BYTE portNumber;
	BYTE pinNumber;
	BYTE bitMask;

	portNumber = GPIO_GET_PORT(pinName);
	pinNumber = GPIO_GET_PIN(pinName);
	bitMask = 1<<pinNumber;

	if( portNumber < TOTAL_PORT_NUMBERS)
	{
		
		*(pPortRenRegs[portNumber]) |= bitMask;
		*(pPortOutRegs[portNumber]) |= bitMask;

	}
}



//******************************************************************************
// Description:      initialize the MSP430 pin-out to default state
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void initMPS430Pins( void )
{

    P1DIR  = PORT_1_DIR_INIT;
    P1OUT  = PORT_1_VALUE_INT;
    P1SEL  = PORT_1_PERIPH_SEL;
        
    P2DIR  = PORT_2_DIR_INIT;
    P2OUT  = PORT_2_VALUE_INT;
    P2SEL  = PORT_2_PERIPH_SEL;
    
    P3DIR  = PORT_3_DIR_INIT;
    P3OUT  = PORT_3_VALUE_INT;
    P3SEL  = PORT_3_PERIPH_SEL;
    
    P4DIR  = PORT_4_DIR_INIT;
    P4OUT  = PORT_4_VALUE_INT;
    P4SEL  = PORT_4_PERIPH_SEL;
    

}  


//******************************************************************************
// Description:      initialize the MSP430 pin-out in power down mode
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SetMPS430PinsInPowerDown( void )
{

    P1DIR  = PORT_1_DIR_POWER_DOWN;
    P1OUT  = PORT_1_VALUE_POWER_DOWN;
    P1SEL  = PORT_1_PERIPH_SEL;
        
    P2DIR  = PORT_2_DIR_POWER_DOWN;
    P2OUT  = PORT_2_VALUE_POWER_DOWN;
    P2SEL  = PORT_2_PERIPH_SEL;
    
    P3DIR  = PORT_3_DIR_POWER_DOWN;
    P3OUT  = PORT_3_VALUE_POWER_DOWN;
    P3SEL  = PORT_3_PERIPH_SEL;
    
    P4DIR  = PORT_4_DIR_POWER_DOWN;
    P4OUT  = PORT_4_VALUE_POWER_DOWN;
    P4SEL  = PORT_4_PERIPH_SEL;
    

}  

