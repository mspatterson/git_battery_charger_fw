/*
 * i2c.c
 *
 *  Created on: 2012-07-20
 *      Author: DD
 */

#include "i2c.h"

#define I2C_READ	1
#define I2C_WRITE	0

#define I2C_TX		1
#define I2C_RX		0

static BOOL Read_nWrite; 	// 0 - write; 1 - read 
static I2C_OBJECT *pI2Cobj;
static BOOL I2C_Tx_nRx;

void i2cB0Initialize(BYTE address)
{
	P3SEL |= (1<<1)|(1<<2);                   // Assign I2C SDA to P3.1 and SCL to P3.2
	UCB0CTL1 |= UCSWRST;                      // Enable SW reset
	UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;     // I2C Master, synchronous mode
	UCB0CTL1 = UCSSEL_2 + UCSWRST;            // Use SMCLK, keep SW reset
//	UCB1BR0 = 80;                             // fSCL = SMCLK/80 = ~100kHz
	UCB0BR0 = 12;                             // fSCL = SMCLK/12 = ~100kHz
//	UCB0BR0 = 10;                             // fSCL = SMCLK/3 = ~330kHz
	UCB0BR1 = 0;
	UCB0I2CSA = address;                      // Slave Address
	UCB0CTL1 &= ~UCSWRST;                     // Clear SW reset, resume operation
}

void i2cWrite(I2C_OBJECT *pI2C_obj)
{
	pI2Cobj = pI2C_obj;
	i2cB0Initialize(pI2Cobj->bySlaveAddress);
	Read_nWrite = I2C_WRITE;
	I2C_Tx_nRx =  I2C_TX;
//	UCB0IE |= UCTXIE;                         // Enable TX interrupt
	IFG2 = 0;
	IE2 |= UCB0TXIE;
	UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
	
}

void i2cRead(I2C_OBJECT *pI2C_obj)
{
	pI2Cobj = pI2C_obj;
	i2cB0Initialize(pI2Cobj->bySlaveAddress);
	Read_nWrite = I2C_READ;
	I2C_Tx_nRx =  I2C_TX;
//	UCB0IE |= UCTXIE;                         // Enable TX interrupt
	IFG2 = 0;
	IE2 |= UCB0TXIE; 						// Enable TX interrupt
	UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
}



#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void)
{
	if (Read_nWrite ==I2C_WRITE)
	{
		//  		if(pI2Cobj->wTxQIndex <= pI2Cobj->wTxQSize)
		if(pI2Cobj->wTxQIndex < pI2Cobj->wTxQSize)
		{
			UCB0TXBUF = *pI2Cobj->ptrTxQ++;
			pI2Cobj->wTxQIndex++;
		}
		else
		{
			UCB0CTL1 |= UCTXSTP;                    // I2C stop condition
			//      		IFG2 &= ~UCB0TXIFG;                     // Clear USCI_B0 TX int flag
			IFG2 = 0;                     // Clear USCI_B0 TX int flag
			IE2 &= ~UCB0TXIE; 						// disable TX interrupt
			pI2Cobj->bTxQSending = FALSE;
		}
	}
	else
	{
		if(I2C_Tx_nRx == I2C_TX)
		{
			if(pI2Cobj->wTxQIndex < pI2Cobj->wTxQSize)
			{
				UCB0TXBUF = *pI2Cobj->ptrTxQ++;
				pI2Cobj->wTxQIndex++;
			}
			else
			{
				pI2Cobj->bTxQSending = FALSE;
				I2C_Tx_nRx = I2C_RX;
				//IFG2 &= ~UCB0TXIFG;                     // Clear USCI_B0 TX int flag
				IFG2 = 0;                     // Clear USCI_B0 TX int flag
				IE2 &= ~UCB0TXIE; 						// disable TX interrupt
				UCB0CTL1 &= ~UCTR;						// set in RX mode
				UCB0CTL1 |= UCTXSTT;  					// I2C start
				IE2 |= UCB0RXIE; 						// Enable RX interrupt
			}
		}
		else
		{
			if(pI2Cobj->wRxQIndex < pI2Cobj->wRxQSize)
			{
				*pI2Cobj->ptrRxQ++ = UCB0RXBUF;
				pI2Cobj->wRxQIndex++;
				if(pI2Cobj->wRxQIndex == (pI2Cobj->wRxQSize -1))	// Only one byte left?
				{
					UCB0CTL1 |= UCTXSTP;                    // Generate I2C stop condition after sending the byte
				}
			}
			if(pI2Cobj->wRxQIndex == pI2Cobj->wRxQSize)// if all bytes has been received
			{
				*pI2Cobj->ptrRxQ = UCB0RXBUF;
				//		IFG2 &= ~UCB0RXIFG;                     // Clear USCI_B0 RX int flag
				IFG2 = 0;                     // Clear USCI_B0 TX int flag
				IE2 &= ~UCB0RXIE; 						// disable TX interrupt
				UCB0STAT &= ~(UCSTPIFG + UCSTTIFG);       // Clear interrupt flags
				pI2Cobj->bRxQReceiving = FALSE;
			}
		}
	}
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void)
{

	if(pI2Cobj->wRxQIndex < pI2Cobj->wRxQSize)
	{
		*pI2Cobj->ptrRxQ++ = UCB0RXBUF;
		pI2Cobj->wRxQIndex++;
		if(pI2Cobj->wRxQIndex == (pI2Cobj->wRxQSize -1))	// Only one byte left?

			UCB0CTL1 |= UCTXSTP;                    // Generate I2C stop condition after sending the byte

	}

	// if all bytes has been received
	if(pI2Cobj->wRxQIndex == pI2Cobj->wRxQSize)
	{
		*pI2Cobj->ptrRxQ = UCB0RXBUF;
//		IFG2 &= ~UCB0RXIFG;                     // Clear USCI_B0 RX int flag
		IFG2 = 0;                     // Clear USCI_B0 TX int flag
		IE2 &= ~UCB0RXIE; 						// disable TX interrupt
		UCB0STAT &= ~(UCSTPIFG + UCSTTIFG);       // Clear interrupt flags
		pI2Cobj->bRxQReceiving = FALSE;
	}
}





