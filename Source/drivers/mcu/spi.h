/*
 * SPI.h
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#ifndef SPI_H_
#define SPI_H_

#include "..\drivers.h"


void SpiB0Initialize(void);
void SpiB0TxRxPacket(BYTE *pDataBuffer, WORD wLength);


#endif /* SPI_H_ */
