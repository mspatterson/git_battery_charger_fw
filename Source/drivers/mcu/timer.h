//************************************************************************
//
//  TIMER.H: Public entry points and data definitions for accessing
//           timer services.
//
//  Copyright (c) 2001, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************


#ifndef _TIMER_H

    #define _TIMER_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "..\drivers.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------


#define MAX_TIMERS      	8
#define MAX_TIMERS_SEC      2

//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------

typedef unsigned int TIMERHANDLE;

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: SysTimerConfig
//
//  Arguments:
//
//  Returns: 
//
//  Description: set TIMER A1 to interrupt every 1msec 
//              
//
//******************************************************************************

void	SysTimerConfig(void);

void	SysTimerConfigB(void);
//******************************************************************************
//
//  Function: SysTimerDisable
//
//  Arguments:
//
//  Returns:
//
//  Description: disable TIMER A1
//
//
//******************************************************************************

void SysTimerDisable(void);
//******************************************************************************
//
//  Function: GetTickCount
//
//  Arguments:
//
//  Returns: number of msec that have passed since the module was initialized
//
//  Description: counts the number of msec since the module was initialized
//              resets to zero after 2^32 msec
//
//******************************************************************************
    DWORD GetTickCount( void );


//******************************************************************************
//
//  Function: GetSystemUpTime
//
//  Arguments:
//
//  Returns: number of secs that have passed since the module was initialized
//
//  Description: counts the number of secs since the module was initialized
//
//******************************************************************************
    DWORD GetSystemUpTime( void );


//******************************************************************************
//
//  Function: GetSystemUTCTime
//
//  Arguments:
//
//  Returns: timer-tick based UTC time
//
//  Description: maintains a UTC timer that gets updated by GPS messages
//
//******************************************************************************
    DWORD GetSystemUTCTime( void );


//******************************************************************************
//
//  Function: GetSystemUTCTime
//
//  Arguments: DWORD new UTC time
//
//  Returns: nothing
//
//  Description: updates the system-based UTC time with the value passed
//
//******************************************************************************
    void SetSystemUTCTime( DWORD newTime );


//******************************************************************************
//
//  Function: WaitMsecs
//
//  Arguments: number of msec to wait
//
//  Returns: nothing
//
//  Description: waits for a given number of msec to pass before continuing on.
//               This function is ment for short waits. Otherwise the system can
//               stall here for up to 65 seconds
//
//******************************************************************************
    void WaitMsecs( WORD msecCount );

//******************************************************************************
//
//  Function: AllocateTimer
//
//  Arguments:  none
//
//  Returns: Timer handle
//
//  Description: allocates a timer to a specific process. Returns NULL if there
//               are no timers available
//
//******************************************************************************
    TIMERHANDLE AllocateTimer( void );

//******************************************************************************
//
//  Function: RegisterTimer
//
//  Arguments:  none
//
//  Returns: Timer handle
//
//  Description: this is an alias for the 'allocateTimer' function which is
//               compatible with legacy code
//
//******************************************************************************
    TIMERHANDLE RegisterTimer( void );

//******************************************************************************
//
//  Function: ResetTimer
//
//  Arguments: timer handle, and reset value in msec
//
//  Returns: nothing
//
//  Description: Initializes the passed timer to the value passed in milliseconds.
//             The timer state is not changed (eg if it was running, it is left running).
//
//******************************************************************************
    void ResetTimer( TIMERHANDLE thTimer, WORD wMillisecs );

//******************************************************************************
//
//  Function: StartTimer
//
//  Arguments:  timer handle
//
//  Returns: nothing
//
//  Description:  Starts the passed timer. Timer counts down from the
//                current/last count of the timer.
//
//******************************************************************************
    void StartTimer( TIMERHANDLE thTimer );

//******************************************************************************
//
//  Function: StopTimer
//
//  Arguments:  timer handle
//
//  Returns: nothing
//
//  Description: Stops the passed timer. Does not clear or reset the timer value.
//
//******************************************************************************
    void StopTimer ( TIMERHANDLE thTimer );

//******************************************************************************
//
//  Function: TimerRunning
//
//  Arguments:  timer handle
//
//  Returns: true/false Is timer running?
//
//  Description: finds the current running state of the specified timer
//
//******************************************************************************
    BOOL TimerRunning( TIMERHANDLE thTimer );


//******************************************************************************
//
//  Function: GetTimeLeft
//
//  Arguments: timer handle
//
//  Returns: the anount of time left on the timer
//
//  Description: calculates how much time is left on a given timer
//              It does not reset, start, or stop the timer though.
//
//******************************************************************************
    WORD GetTimeLeft( TIMERHANDLE thTimer );

//******************************************************************************
//
//  Function: TimerExpired
//
//  Arguments: timer handle
//
//  Returns: true/false Is the timer expired?
//
//  Description: calculates if a given timer has reached 0
//
//******************************************************************************
    BOOL TimerExpired( TIMERHANDLE thTimer );

//******************************************************************************
//
//  Function: IsTimerHandleValid
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Returns: true/false Is handle valid?
//
//  Description:  Returns TRUE if the passed handle is valid, FALSE otherwise.
// Handle is invalid if it falls outside the timer info array range.
//
//******************************************************************************
BOOL IsTimerHandleValid( TIMERHANDLE thAHandle );


//******************************************************************************
//
//  Function: InitTimers
//
//  Arguments: function pointer for System Tick callback
//
//  Returns: nothing
//
//  Description: Initialises timers, must be called before any other function in
//              this module. This function assumes that interrupts can be hooked.
//              The SysTick interrupt is configured and used for timer updates.
//
//******************************************************************************
void InitTimers( void  );

//******************************************************************************
//
//  Function: UnregisterTimer
//
//  Arguments:  Timer Handle
//
//  Returns: BOOL
//
//  Description: unregister timer
//               
//
//******************************************************************************
BOOL UnregisterTimer( TIMERHANDLE theHandle);



WORD GetTenMsCount (void);


TIMERHANDLE AllocateSecTimer( void );

TIMERHANDLE RegisterSecTimer( void );

BOOL UnregisterSecTimer( TIMERHANDLE theHandle);

void StartSecTimer( TIMERHANDLE thTimer );

void ResetSecTimer( TIMERHANDLE thTimer, WORD wSecs );

BOOL SecTimerExpired( TIMERHANDLE thTimer );

BOOL IsSecTimerHandleValid( TIMERHANDLE thAHandle );

void SetTimerOut(void);

void ResetTimerOut(void);

#endif  // _TIMER_H













