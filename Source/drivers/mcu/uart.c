/*
 * uart.c
 *
 *  Created on: 2013-04-25
 *      Author: Owner
 */

#include "uart.h"

static UART_OBJECT uartObj;

/*
 * SMCLK(comming from DCO, div =0) on P2.1 measured 1.136MHZ .
 * with UCA0BR0 = 4 the UART speed is 284Kbs.
 */

void UARTA0Init(void)
{
	UCA0CTL1 |= UCSWRST;                     // **Initialize USCI state machine**
	P3SEL |= 0x30;                             // P3.4,5 = USCI_A0 TXD/RXD
	UCA0CTL1 |= UCSSEL_2;                     // SMCLK
	UCA0BR0 = 8;                              // 1MHz 115200
//	UCA0BR0 = 4;                              // 1.136MHz 284000
	UCA0BR1 = 0;                              // 1MHz 115200
	UCA0MCTL = UCBRS2 + UCBRS0;               // Modulation UCBRSx = 5
	UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
}


void UARTA0RxEnable(void)
{
	IE2 |= UCA0RXIE;
}


void UARTA0TxEnable(void)
{
	IE2 |= UCA0TXIE;
}

void UARTA0RxDisable(void)
{
	IE2 &= ~UCA0RXIE;
}


void UARTA0TxDisable(void)
{
	IE2 &= ~UCA0TXIE;
}


BOOL UARTA0SendBufferInt(const BYTE* pBuffer, WORD wLength )
{
	// ensure that a tx queue isn't already in progress
    if( uartObj.bTxQSending == TRUE )
        return FALSE;

	// set the queue tracking vars to track this new Tx Queue being posted here
    uartObj.ptrTxQ = pBuffer;
    uartObj.wTxQIndex = 0;
    uartObj.wTxQSize = wLength;
    uartObj.bTxQSending = TRUE;
	
	//UARTA1TxEnable();
	//call the TX ISR explicitly 
	uart0TxISR();

	UARTA0TxEnable();
	// enable interrupts
	
	return TRUE;
}



void UARTA0SendBufferPol(const BYTE* pBuffer, WORD wLength )
{
	unsigned int i;
//	EnTXRS485();	// enable RS485 TX driver
	P3OUT |=BIT7;
	for(i=0;i<wLength;i++)
	{
		UCA0TXBUF = pBuffer[i];                   // TX
		while (!(IFG2&UCA0TXIFG));                // USCI_A0 TX buffer ready?
//		UCA0TXBUF = pBuffer[i];                   // TX
	}
//	while (!(IFG2&UCA0TXIFG));                // wait to transmit the last byte
//	DisableRS485();
	__delay_cycles(100);
	P3OUT &=~BIT7;
}




void uart0TxISR( void )
{
    // This function is called in the context of an ISR
    if( uartObj.wTxQIndex < uartObj.wTxQSize )
    {
    	UCA0TXBUF = uartObj.ptrTxQ[uartObj.wTxQIndex];
        uartObj.wTxQIndex++;

    }
    else
    {
        // Disable TX interrupt
        UARTA0TxDisable();
        UARTA0RxEnable();
        uartObj.bTxQSending = FALSE;


    }
}


//void atZbRxISR( void )
//{
//
//	if( wZbRxIndex < MAX_RESP_LINE_LEN )
//    {
//    	byZbRxBuffer[wZbRxIndex] = UCA1RXBUF;
//        ++wZbRxIndex;
//	//	ResetTimer( thZbStateTimer, RECEIVE_DATA_TIMEOUT );
//    }
//
//}


//******************************************************************************
//
//  Function: IsZbTiSending
//
//  Arguments: none
//
//  Returns: TRUE/FALSE.
//
//  Description: return the TX sending status
//
//******************************************************************************

BOOL IsZbTiSending(void)
{
	return uartObj.bTxQSending;
}





