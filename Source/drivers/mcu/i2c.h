/*
 * SPI.h
 *
 *  Created on: 2012-07-20
 *      Author: Owner
 */
#ifndef I2C_H_
#define I2C_H_

#include "..\drivers.h"


typedef struct
{
	BYTE  bySlaveAddress;
   	BYTE* ptrTxQ;                           
   	WORD  wTxQIndex;                              
    WORD  wTxQSize;
    BOOL  bTxQSending;
    BYTE* ptrRxQ;
    WORD  wRxQIndex;                              
    WORD  wRxQSize;
    BOOL  bRxQReceiving;

} I2C_OBJECT;


void i2cB1Initialize(BYTE address);
void i2cWrite(I2C_OBJECT *pI2C_obj);
void i2cRead(I2C_OBJECT *pI2C_obj);


#endif /*I2C_H_*/
