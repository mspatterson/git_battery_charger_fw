#ifndef GPIO_H_
#define GPIO_H_



#include "..\drivers.h"

// Date JUNE 2013 - initial release for PCB-00620-06-1v0

//******************************************************************************
// Port 1	-
//******************************************************************************
// P1.0 - TP32			[Output]
// P1.1 - TP31			[Output]
// P1.2 - BUZZER		[Output]
// P1.3 - TP28			[Output]
// P1.4 - TCK		   	[Input]	- Debug Port
// P1.5 - TMS		 	[Input] - Debug Port
// P1.6 - TDI		 	[Input] - Debug Port
// P1.7 - TDO			[Output]- Debug Port
#define TP32			0x00
#define TP31			0x01
#define BUZZER			0x02
#define TP28			0x03
#define DBG_TCK			0x04
#define DBG_TMS			0x05
#define DBG_TDI			0x06
#define DBG_TDO			0x07

#define PORT_1_DIR_INIT     0x0F
#define PORT_1_VALUE_INT    0x00
#define PORT_1_PERIPH_SEL   0x00

// in POWER DOWN MODE set all pins as inputs
#define PORT_1_DIR_POWER_DOWN		0x00
#define PORT_1_VALUE_POWER_DOWN    	0x00


//******************************************************************************
// Port 2	-
//******************************************************************************
// P2.0 - TP34			[Output]
// P2.1 - TP35			[Output]
// P2.2 - ADC_DRDY		[Input]
// P2.3 - TP44			[Output]
// P2.4 - TP33			[Output]
// P2.5 - NC1			[Output]
// P2.6 - XIN			[Input] - Xtal port
// P2.7 - XOUT			[Output]- Xtal port
#define TP34			0x10
#define TP35			0x11
#define ADC_DRDY		0x12
#define TP44			0x13
#define TP33			0x14
#define NC1				0x15
#define XIN				0x16
#define XOUT			0x17


#define PORT_2_DIR_INIT     0x3B
#define PORT_2_VALUE_INT    0x00
#define PORT_2_PERIPH_SEL  	0xC0		// Software must not clear the P2SELx 6,7 bits if crystal operation is required.
// in POWER DOWN MODE set ...
#define PORT_2_DIR_POWER_DOWN     0x3B
#define PORT_2_VALUE_POWER_DOWN    0x00

  
//******************************************************************************
// Port 3	-
//******************************************************************************
// P3.0 - ADC_CSn	    [Output]
// P3.1 - ADC_DIN		[Output] 	SPI out
// P3.2 - ADC_DOUT		[Input]   	SPI in
// P3.3 - ADC_CLK		[Output]	SPI clk
// P3.4 - TP50			[Output]
// P3.5 - TP47			[Output]
// P3.6 - TP46			[Output]
// P3.7 - TP45			[Output]
#define ADC_CSn			0x20
#define ADC_DIN			0x21
#define ADC_DOUT		0x22
#define ADC_CLK			0x23
#define TP50			0x24
#define TP47			0x25
#define TP46			0x26
#define TP45			0x27

#define PORT_3_DIR_INIT     0xFB
#define PORT_3_VALUE_INT    0x01
#define PORT_3_PERIPH_SEL   0x00
// in POWER DOWN MODE set ...
#define PORT_3_DIR_POWER_DOWN     	0xFB
#define PORT_3_VALUE_POWER_DOWN    	0x01

   
//******************************************************************************
// Port 4	-
//******************************************************************************
// P4.0 - BAT_SW	 	[Output]	- Turn on before measure  BAT_DETECT
// P4.1 - VFB_SW	 	[Output]	- Turn on before measure  VFB
// P4.2 - CHRG_RATE 	[Output]	- Switch charge rate: low -50mA, high - 370mA
// P4.3 - CHRG_EN	 	[Output]	- Enable charging
// P4.4 - TP49			[Output]
// P4.5 - LED_OUT		[Output]
// P4.6 - TP38			[Output]
// P4.7 - TP37			[Output]
#define BAT_SW			0x30
#define VFB_SW			0x31
#define CHRG_RATE		0x32
#define CHRG_EN			0x33
#define TP49			0x34
#define LED_OUT			0x35
#define TP38			0x36
#define TP37			0x37

#define PORT_4_DIR_INIT     		0xFF
#define PORT_4_VALUE_INT    		0x00
#define PORT_4_PERIPH_SEL			0x00
// in POWER DOWN MODE set all pins as output low
#define PORT_4_DIR_POWER_DOWN		0xFF
#define PORT_4_VALUE_POWER_DOWN		0X00

#define TOTAL_PORT_NUMBERS	4


// get the port number for the pin name
#define GPIO_GET_PORT( a )	( (a) >> 4 )

// get the pin number for the pin name4
#define GPIO_GET_PIN( a )	( (a) & 0x0f )


void SetPinAsPeripheral(BYTE pinName);
void SetPinAsOutput(BYTE pinName);
void SetPinAsInput(BYTE pinName);
void SetOutputPin(BYTE pinName, BOOL bState);
BOOL ReadInputPin(BYTE pinName);
void EnableInputPullUp(BYTE pinName);

void initMPS430Pins( void );
void SetMPS430PinsInPowerDown( void );


#endif /*GPIO_H_*/
