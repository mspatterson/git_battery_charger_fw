/*
 * spi.c
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#include "spi.h"


//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			SpiB0Initialize(void)
//!
//!	\brief		initialize the SPI interface
//!
//!	\note
//!
//!	\param[in]	void
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void SpiB0Initialize(void)
{
	SetPinAsPeripheral(ADC_DIN);
	SetPinAsPeripheral(ADC_DOUT);
	SetPinAsPeripheral(ADC_CLK);
//	SetPinAsInput(ADC_DIN);

	UCB0CTL1 |= UCSWRST; // **Put state machine in reset**
	UCB0CTL0 |= UCMSB + UCMST + UCSYNC;       // 3-pin, 8-bit SPI mstr, MSB 1st
	UCB0CTL1 |= UCSSEL_2;                     // SMCLK
	UCB0BR0 = 0x04;
	UCB0BR1 = 0;
	UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**

}

//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			SpiB0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
//!
//!	\brief		writes and reads bytes on the SPI interface
//!
//!	\note 		the chip must be selected from the calling function
//!
//!	\param[in]	pDataBuffer		pointer to Data to write and then store the response
//!
//!	\param[in]	wLength			Number of bytes to w/r
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void SpiB0TxRxPacket(BYTE *pDataBuffer, WORD wLength)
{

	WORD cnt;
	for( cnt = 0; cnt < wLength; cnt++ )
	{
		while (!(IFG2 & UCB0TXIFG)); 	// USCI_B0 TX buffer ready?

		UCB0TXBUF = pDataBuffer[cnt];

		while (!(IFG2 & UCB0RXIFG)); 	// USCI_B0 RX buffer ready?

		pDataBuffer[cnt] = UCB0RXBUF;
	}

//	while (!(IFG2 & UCB0RXIFG));

}



