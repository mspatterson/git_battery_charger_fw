//******************************************************************************
//
//  DataManager.c: data management routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-03-13     DD          Initial Version
//
//******************************************************************************

#include "DataManager.h"


const WORD SegmentAddress[4]={ADDRESS_INFO_A, ADDRESS_INFO_B, ADDRESS_INFO_C, ADDRESS_INFO_D};

BYTE RamArray[BYTES_IN_SEGMENT+2];


//******************************************************************************
//
//  Function: WritePageInFlash
//
//  Arguments: page number, pointer to the data to be written , number of bytes
//
//  Returns: TRUE/FALSE
//
//  Description: write 'length' number of bytes to the information memory
//
//******************************************************************************
BYTE WritePageInFlash(BYTE page, BYTE *pData, WORD length)
{
	WORD wData;
	WORD i,index;

	if(page<PROTECTED_PAGES)
		return 1;
	if(page>=MAX_NUMBER_PAGES)
		return 2;

	if( length != BYTES_IN_PAGE )
		return 3;

	// find to which segment the page belongs
	wData = page / PAGES_IN_SEGMENT ;

	// copy the whole segment in RAM
	ReadFlash(SegmentAddress[wData], RamArray, (WORD) BYTES_IN_SEGMENT);

	index = (page % PAGES_IN_SEGMENT) * BYTES_IN_PAGE;

	// copy the data in the corresponding place
	for(i=0;i<length;i++)
	{
		RamArray[index++] = *pData++;
	}

	// write the ram buffer in flash, the function erase the flash before writing
	WriteFlash(SegmentAddress[wData], RamArray, (WORD) BYTES_IN_SEGMENT);

	return 0;
}

//******************************************************************************
//
//  Function: WritePageInFlash
//
//  Arguments: page number, pointer to store the data , number of bytes
//
//  Returns: TRUE/FALSE
//
//  Description: reads 'length' number of bytes from the information memory
//
//******************************************************************************
BYTE ReadPageFromFlash(BYTE page, BYTE *pData, WORD length)
{
	WORD wData;
	WORD i,index;

	if(page>=MAX_NUMBER_PAGES)
			return 2;

	if( length > BYTES_IN_PAGE )
		return 3;

	// find to which segment the page belongs
	wData = page / PAGES_IN_SEGMENT ;

	// copy the whole segment in RAM
	ReadFlash(SegmentAddress[wData], RamArray, (WORD) BYTES_IN_SEGMENT);

	index = (page % PAGES_IN_SEGMENT) * BYTES_IN_PAGE;

	// copy the data from the corresponding place
	for(i=0;i<length;i++)
	{
		*pData++ = RamArray[index++];
	}

	return 0;
}


















//
//
////******************************************************************************
//// Function name:    InitializeDataInRam
////******************************************************************************
//// Description:      initialise the RamTable from flash
////					if the flash is empty set the default values
//// parameters:       none
////
//// Returned value:   none
////
////******************************************************************************
//void InitializeDataInRam(void)
//{
//	WORD i;
//	DATA_STRUCT TempDataTable;
//
////	ReadFlash((BYTE*)&TempDataTable, sizeof(TempDataTable));
//
//	if (TempDataTable.Header == 0xFFFF)		// FLASH is empty set to default values
//	{
//		SetDefaultValues();
//	}
//	else
//	{
//		// may need to add CRC check - if wrong CRC do what? -
//		// or report all parameters to the PC software
////		if(TempDataTable.DataCRC != CalculateCRC((BYTE *)&TempDataTable, sizeof(TempDataTable)-1))
////		{
////			SetDefaultValues();
////		}
//
//		RamDataTable.Header = TempDataTable.Header ;
//
//		for (i = 0; i<NUM_OF_DATA;i++)
//		{
//			RamDataTable.DataTable[i]= TempDataTable.DataTable[i];
//		}
//
//		// calculate the CRC on all bytes except the packet except the CRC byte.
//		RamDataTable.DataCRC = CalculateCRC((BYTE *)&RamDataTable, sizeof(RamDataTable)-1);
//
//	}
//
//}
//
////******************************************************************************
//// Function name:    SetDefaultValues
////******************************************************************************
//// Description:      load the default values into the Ram table
////
//// parameters:       none
////
//// Returned value:   none
////
////******************************************************************************
//
//void SetDefaultValues(void)
//{
//	WORD i;
//	RamDataTable.Header = DATA_HEADER_IN_FLASH;
//
//	for (i = 0; i<NUM_OF_DATA;i++)
//	{
//		RamDataTable.DataTable[i]=DefaultDataTable[i];
//	}
//
//	// calculate the CRC on all bytes except the packet except the CRC byte.
//	RamDataTable.DataCRC = CalculateCRC((BYTE *)&RamDataTable, sizeof(RamDataTable)-1);
//
//	// write the data in flash
//	WriteFlash((BYTE *)&RamDataTable, sizeof(RamDataTable));
//}
//
////******************************************************************************
//// Function name:    WriteVariable
////******************************************************************************
//// Description:      write the wValue into wParamNumber parameter
////
//// parameters:       parameter number and the value to be written
////
//// Returned value:   TRUE/FALSE
////
////******************************************************************************
//
//BOOL WriteVariable(WORD wParamNumber, WORD wValue)
//{
//	if(wParamNumber>=NUM_OF_DATA)
//		return FALSE;
//
//	RamDataTable.DataTable[wParamNumber]= wValue;
//
//	// calculate the CRC on all bytes except the packet except the CRC byte.
//	RamDataTable.DataCRC = CalculateCRC((BYTE *)&RamDataTable, sizeof(RamDataTable)-1);
//
//	// write the data in flash
//	WriteFlash((BYTE *)&RamDataTable, sizeof(RamDataTable));
//
//	return TRUE;
//}
//
////******************************************************************************
//// Function name:    ReadVariable
////******************************************************************************
//// Description:      read parameter
////
//// parameters:       parameter number and pointer to data
////
//// Returned value:   TRUE/FALSE
////
////******************************************************************************
//
//BOOL ReadVariable(WORD wParamNumber, WORD *pValue)
//{
//	if(wParamNumber>=NUM_OF_DATA)
//			return FALSE;
//
//	*pValue = RamDataTable.DataTable[wParamNumber];
//
//	return TRUE;
//}
//
//
//
