/*
 * ads1118.h
 *
 *  Created on: 2013-06-05
 *      Author: Owner
 */

#ifndef ADS1118_H_
#define ADS1118_H_

#include "..\drivers.h"

//! Total number of ADC channels available
#define ADC_EXT_MAX_CHANNELS	4

//! Bit resolution of the ADC
#define AD1118_BIT_RESOLUTION		16

//! Sample resolution of the ADC
#define ADS1118_RESOLUTION			(1<<AD1118_BIT_RESOLUTION)



#define ADS1118_SS_START			1<<15		//SS: Single-shot conversion start
#define ADS1118_SS_MODE				1<<8		//Power-down and single-shot mode (default)
#define ADS1118_CC_MODE				0<<8		//Continuous conversion mode
#define ADS1118_ADC_MODE			0<<4		//0 = ADC mode (default)
#define ADS1118_TEMP_MODE			1<<4		//1 = Temperature sensor mode
#define ADS1118_PULLUP_EN			1<<3		//1 = Pull-up resistor enabled on DOUT/DRDY pin (default)
#define ADS1118_WR					1<<1

/*Config Register
 * Bit 15 SS: Single-shot conversion start
	This bit is used to start a single conversion. SS can only be written when in power-down state and has no
	effect when a conversion is ongoing.
	When writing:
	0 = No effect
	1 = Start a single conversion (when in power-down state)
	Always reads back as '0' (default).

Bits[14:12] MUX[2:0]: Input multiplexer configuration
	These bits configure the input multiplexer.
	000 = AINP is AIN0 and AINN is AIN1 (default) 	100 = AINP is AIN0 and AINN is GND
	001 = AINP is AIN0 and AINN is AIN3 			101 = AINP is AIN1 and AINN is GND
	010 = AINP is AIN1 and AINN is AIN3 			110 = AINP is AIN2 and AINN is GND
	011 = AINP is AIN2 and AINN is AIN3 			111 = AINP is AIN3 and AINN is GND

Bits[11:9] PGA[2:0]: Programmable gain amplifier configuration
	These bits configure the programmable gain amplifier.
	000 = FS is �6.144 V			100 = FS is �0.512 V
	001 = FS is �4.096 V	 		101 = FS is �0.256 V
	010 = FS is �2.048 V (default) 	110 = FS is �0.256 V
	011 = FS is �1.024 V 			111 = FS is �0.256 V

Bit 8 MODE: Device operating mode
	This bit controls the ADS1118 operating mode.
	0 = Continuous conversion mode
	1 = Power-down and single-shot mode (default)

Bits[7:5] DR[2:0]: Data rate -These bits control the data rate setting.
	000 = 8 SPS 	100 = 128 SPS (default)
	001 = 16 SPS 	101 = 250 SPS
	010 = 32 SPS 	110 = 475 SPS
	011 = 64 SPS 	111 = 860 SPS

Bit 4 TS_MODE: Temperature sensor mode
	This bit configures the ADC to convert temperature or input signals.
	0 = ADC mode (default)
	1 = Temperature sensor mode

Bit 3 PULL_UP_EN: Pull-up enable
	This bit enables a weak pull-up resistor on the DOUT/DRDY pin. When enabled, a 400-kOhm resistor connects
	the bus line to supply. When disabled, the DOUT/DRDY pin floats.
	0 = Pull-up resistor disabled on DOUT/DRDY pin
	1 = Pull-up resistor enabled on DOUT/DRDY pin (default)

Bits[2:0] NOP: No operation
	The NOP bits control whether data are written to the Config register or not. In order for data to be written to the
	Config register, the NOP bits must be '01'. Any other value results in a NOP command. DIN can be held high or
	low during SCLK pulses without data being written to the Config register.
	00 = Invalid data, do not update the contents of the Config register
	01 = Valid data, update the Config register (default)
	10 = Invalid data, do not update the contents of the Config register
	11 = Invalid data, do not update the contents of the Config register

Bit 0 Not used
	Always reads '1'

*/

// Analog input sources
// DO NOT CHANGE THESE VALUES! THEY CORRESPOND TO ACTUAL REGISTER BITS!
typedef enum ADS1118_INPUT_ENUM
{
	// 000: AIN0:AIN1
	ADS1118_IN_AIN0_AIN1 		= 0<<12,

	// 001: AIN0:AIN3
	ADS1118_IN_AIN0_AIN3		= 1<<12,

	// 010: AIN1:AIN3
	ADS1118_IN_AIN1_AIN3		= 2<<12,

	// 011: AIN2:AIN3
	ADS1118_IN_AIN2_AIN3		= 3<<12,

	// 100: AIN0:GND
	ADS1118_IN_AIN0				= 4<<12,

	// 101: AIN1:GND
	ADS1118_IN_AIN1				= 5<<12,

	// 110: AIN2:GND
	ADS1118_IN_AIN2				= 6<<12,

	// 111: AIN3:GND
	ADS1118_IN_AIN3				= 7<<12,

	ADS1118_MAX_CHANNELS

} ADS1118_SOURCE;



//! Defines allowed sensitivity settings
typedef enum ADS1118_GAIN_ENUM_ {

	//FS is �6.144
	ADS1118_GAIN_0				= 0<<9,

	//FS is �4.096 V
	ADS1118_GAIN_1				= 1<<9,

	//FS is �2.048 V
	ADS1118_GAIN_2				= 2<<9,

	//FS is �1.024 V
	ADS1118_GAIN_3				= 3<<9,

	//FS is �0.512 V
	ADS1118_GAIN_4				= 4<<9,

	//FS is �0.256 V
	ADS1118_GAIN_5				= 5<<9,

	//FS is �0.256 V
	ADS1118_GAIN_6				= 6<<9,

	//FS is �0.256 V
	ADS1118_GAIN_7				= 7<<9,

	ADC_GAIN_UNKNOWN

} ADS1118_GAIN ;


//! Defines allowed sampling rate
typedef enum ADS1118_RATE_ENUM_ {

	ADS1118_RATE_8_HZ			=0<<5,

	ADS1118_RATE_16_HZ			=1<<5,

	ADS1118_RATE_32_HZ			=2<<5,

	ADS1118_RATE_64_HZ			=3<<5,

	ADS1118_RATE_128_HZ			=4<<5,

	ADS1118_RATE_250_HZ			=5<<5,

	ADS1118_RATE_475_HZ			=6<<5,

	ADS1118_RATE_860_HZ			=7<<5

} ADS1118_RATE ;



typedef enum ADC_IN_CHANNEL_ENUM
{
	ISENSE = 0,
	VFB = 1,
	INPUT_V = 2,
	BAT_DETECT = 3,
	MAX_IN_CHANNELS

} ADC_IN_CHANNEL;


BOOL ADS1118ReadVoltage(ADC_IN_CHANNEL inChannel, UVOLTS *p_vResult);

BOOL ADS1118HasResult(void);
void ADS1118ReadTempCont(int *p_Result);
SBYTE GetTemperature(void);

#endif /* ADS1118_H_ */
