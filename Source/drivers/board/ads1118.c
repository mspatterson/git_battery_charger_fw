/*
 * ADS1118.c
 *
 *  Created on: 2012-05-18
 *      Author: Owner
 */

#include "ads1118.h"



//! ADS1118 full range
#define ADS1118_FULL_RANGE 		0x7FFF
#define ADC_BASIC_CONFIG	ADS1118_GAIN_2|ADS1118_RATE_64_HZ|ADS1118_SS_MODE|ADS1118_SS_START|ADS1118_PULLUP_EN|ADS1118_WR
#define ADC_CH0_CONFIG		ADS1118_IN_AIN0|ADC_BASIC_CONFIG
#define ADC_CH1_CONFIG		ADS1118_IN_AIN1|ADC_BASIC_CONFIG
#define ADC_CH2_CONFIG		ADS1118_IN_AIN2|ADC_BASIC_CONFIG
#define ADC_CH3_CONFIG		ADS1118_IN_AIN3|ADC_BASIC_CONFIG

static signed long tempRawData;
static SBYTE adcTemperature;


static const WORD adcConfigSettings[ADC_EXT_MAX_CHANNELS] = {ADC_CH0_CONFIG,ADC_CH1_CONFIG,ADC_CH2_CONFIG,ADC_CH3_CONFIG};





/////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			BOOL		ADS1118ReadVoltage(BYTE bSlaveSelect, ADC_EXT_SOURCE	analogChannel, UVOLTS *p_vResult )
//!
//!	\brief		Takes a sample and converts it to microvolts.
//!
//!	\param[in]	analogChannel		Specifies the analog channel to configure.
//!
//!	\param[out]	p_vResult			Pointer to the destination to store the result in uV
//!
//!	\return		BOOLEAN
//!				- TRUE if successful. The result is now stored in p_vResult.
//!				- FALSE if unsuccessful.
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
BOOL ADS1118ReadVoltage(ADC_IN_CHANNEL inChannel, UVOLTS *p_vResult)
{

	BYTE bPacket[5];
	WORD config;
	WORD rawResult;

	const long int iAdcFactorFor2048uV = (2048000 / ADS1118_FULL_RANGE); // check!

	if (inChannel > MAX_IN_CHANNELS)
		return FALSE;

	SetOutputPin(ADC_CSn , FALSE );
	__delay_cycles(100);

	config = adcConfigSettings[inChannel];

	bPacket[0] = (BYTE)(config>>8);
	bPacket[1] = (BYTE)config;
	bPacket[2] = (BYTE)(config>>8);
	bPacket[3] = (BYTE)config;

	SpiB0TxRxPacket(bPacket, 4);

	__delay_cycles(15000);
	while(!ADS1118HasResult());

	config = 0x7F & config;			// clear the start bit
	bPacket[0] = (BYTE)(config>>8);
	bPacket[1] = (BYTE)config;
	bPacket[2] = (BYTE)(config>>8);
	bPacket[3] = (BYTE)config;

	SpiB0TxRxPacket(bPacket, 4);

	__delay_cycles(100);
	SetOutputPin(ADC_CSn , TRUE );

	rawResult = (WORD)bPacket[0];
	rawResult = (rawResult<<8)|(WORD)bPacket[1];

	*p_vResult = (UVOLTS)rawResult;
	*p_vResult *= iAdcFactorFor2048uV;
	
	return TRUE;
}






BOOL ADS1118HasResult(void)
{
	if( (ReadInputPin(ADC_DRDY )) == FALSE )
		return TRUE;
	else
		return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			BOOL		ADS1118ReadVoltage(BYTE bSlaveSelect, ADC_EXT_SOURCE	analogChannel, UVOLTS *p_vResult )
//!
//!	\brief		Takes a sample and converts it to microvolts.
//!
//!	\param[in]	adcNumber			Specifies the ADC number.
//!
//!	\param[out]	p_vResult			Pointer to the destination to store the result in uV
//!
//!	\return		BOOLEAN
//!				- TRUE if successful. The result is now stored in p_vResult.
//!				- FALSE if unsuccessful.
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void ADS1118ReadTempCont(int *p_Result)
{

//	BYTE bResult[5];
//	volatile signed long iSample;
//	long int iTemp;
//	volatile DWORD dwResult;
//
//	const long int iAdcTempCoefficient = 30517;
//
//	if (adcNumber >= ADS1118_TOTAL)
//		return FALSE;
//
//
//
//	if (dwResult>ADS1118_FULL_RANGE)
//	{
//		iSample = (signed long)(dwResult|0xFF000000);
//	}
//	else
//	{
//		iSample = (signed long)dwResult;
//	}
//
//	iSample /= 1000;
//	iSample *= iAdcTempCoefficient;
//	iSample /= 1000;
//
//	*p_vResult = iSample;

}



SBYTE GetTemperature(void)
{
	// to do convert the raw data in temperature
	// 0.5�C per count starting at 0 (-64�C to +63.5�C)

	adcTemperature = (SBYTE)tempRawData/100; // check

	return adcTemperature;
}


